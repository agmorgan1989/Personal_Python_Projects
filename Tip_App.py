# Calculating Tip App
# Author: Aaron Morgan
# Date and Version: 18/12/2020 V1.1

print("Hello, welcome to the Tip Calculater app.")
bill_amount = input("Please enter the full bill amount: ")
bill_amount = float(bill_amount)
if bill_amount <= 0:
    print("The bill must be greater than 0. Please try again")
    quit()

print("The bill amount is", bill_amount)

print("The average tip amount given is usually 20%")
tip_percent = input("Please enter the tip percent you would like to give: ")
tip_percent = float(tip_percent)
if tip_percent >= 100 or tip_percent <= 0:
    print("The amount must be between 1% and 99%. Please try again")
    quit()

tip = bill_amount / tip_percent
tip = round(tip, 2)
total = bill_amount + tip
total = round(total, 2)

print("The tip you should leave is: ", tip)

print("The full amount to leave is: ", total)
# Bingo Game
# Author: Aaron Morgan
# Date and Version: 16/12/2020 V1

from time import sleep
import random

# Initial empty lists for bingo cards.
player_one = []
player_two = []
player_three = []
player_four = []

# Function for creating a bingo card with 6 numbers.

def bingo_card(player):
    while len(player) < 6:
        number = random.randint(1,90)
        if number in (player):
            continue
        else:
            player.append(number)
    player.sort()
    print(player)

# Introduction to the game and asks user for the amount of players between 2 and 4.

print("Welcome to this game of bingo!\nEach player has 6 balls.\nFirst one to have all their balls called WINS!")
player = input("How many players are playing today please enter a number between 2 and 4: ")

# Calls the function bingo_card and creates bingo cards for the amount of players.

if player == "2":
    bingo_card(player_one)
    name_1 = input("Please enter Player 1's name: ")
    bingo_card(player_two)
    name_2 = input("Please enter Player 2's name: ")
    print("Welcome to the game ", name_1, "and", name_2)
elif player == "3":
    name_1 = input("Please enter Player 1's name: ")
    bingo_card(player_two)
    name_2 = input("Please enter Player 2's name: ")
    bingo_card(player_three)
    name_3 = input("Please enter Player 3's name: ")
    print("Welcome to the game ", name_1, "and", name_2, "and", name_3)
elif player == "4":
    name_1 = input("Please enter Player 1's name: ")
    bingo_card(player_two)
    name_2 = input("Please enter Player 2's name: ")
    bingo_card(player_three)
    name_3 = input("Please enter Player 3's name: ")
    bingo_card(player_four)
    name_4 = input("Please enter Player 4's name: ")
    print("Welcome to the game ", name_1, "and", name_2, "and", name_3, "and", name_4)
else:
    print("I'm sorry, you have entered an invalid selection please start again")
    quit()


# User is asked for the speed in which they would like the balls to be called and to press enter to start the game

speed = input("Please enter the speed in seconds you would like each ball to be called: ")
input("When ready please enter to start the game: ")

# Initial variables for bingo caller.

numbers_called = []
number = 0

# Bingo caller - while the number of balls called is below 90 another ball will be called.

while len(numbers_called) < 90:
    number = random.randint(1,90)
    if number in numbers_called:
        continue
    else:
        numbers_called.append(number)
        sleep(int(speed))
        print("The next ball is: ", number)

        # If player has the number then this number will be removed from their list.
        # The game will tell them how many balls they need and what they are.
        # If all the players balls come out then they are declared the winner and the script stops.

        if number in player_one:
            player_one.remove(number)
            if len(player_one) == 0:
                print("BINGO!", name_1, "wins")
                break
            else:
                print(name_1, "has", len(player_one), "numbers left, these are", player_one)

        elif number in player_two:
            player_two.remove(number)
            if len(player_two) == 0:
                print("BINGO!", name_2, "Wins!")
                break
            else:
                print(name_2, "has", len(player_two), "numbers left, these are", player_two)

        elif number in player_three:
            player_three.remove(number)
            if len(player_three) == 0:
                print("BINGO!", name_3, "wins")
                break
            else:
                print(name_3, "has", len(player_three), "numbers left, these are", player_three)

        elif number in player_four:
            player_four.remove(number)
            if len(player_four) == 0:
                print("BINGO!", name_4, "wins!")
                break
            else:
                print(name_4, "has", len(player_four), "numbers left, these are", player_four)
        else:
            continue
            
# Password Generator - This program may randomly assign a password or scramble a keyword between 8 and 15 characters
# Author: Aaron Morgan
# Date and Version: 11/01/2021 V1.0

import random

# Welcome statement to the program followed by initial options asking for user input

print("Welcome to the password generator")
user = input("If you would like a randomly generated password please enter 'random'" 
        "\nif you would like to use a keyword which can be shuffled, please enter 'keyword'"
        "\nPlease Note: keyword must be at least 8 characters long and no longer than 12 characters!: ")

# Random option - Creates a 16 character random password from the characters in password_list.
# Can call the same character more than once.

if user == "random":
    random_password = ""

    password_list = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
                     '1','2','3','4','5','6','7','8','9','0','!','-','+','?','/']

    while len(random_password) < 16:
        character = random.choice(password_list)
        random_password = random_password + character

    print("Your randomly generated 16 digit password is:", random_password)

# Keyword option - Creates a scrambled version of the keyword entered by the user.
# Keyword must be at least 8 characters long and have no more than 12 characters.

elif user == "keyword":

    # Intialises variable - Places each seperate character into a list ready to be shuffled
    keyword_password = ""
    keyword = input("Please enter a keyword, (this keyword must be at least 8 characters long and have no more than 12 characters): ")
    keyword_list = list(keyword)

    # Checks to see if the new list is between 8 and 12 characters long.
    # If this returns true, the list is shuffled. The list is then turned into a string for readability purposes.

    if len(keyword_list) >= 8 and len(keyword_list) <= 12:
      random.shuffle(keyword_list)
      keyword_string = "".join(keyword_list)
      print("Your new shuffled keyword password is:", keyword_string)
    
    elif len(keyword) < 8:
        print("You have entered a keyword less than 8 characters, please restart.")
        exit
    else:
        print("You have entered a keyword more than 12 characters, please restart.")
        exit

# If user does not enter a correct option, the program shows a message and stops.

else:
    print("You have entered an incorrect command. \nYou must enter either 'random' or 'keyword'. \nPlease restart")